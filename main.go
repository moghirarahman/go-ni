package main

import (
	"log"
	
	"github.com/ni/api"
)
func main() {
	server, err := api.NewServer()
	if err != nil {
		log.Fatal(err)
	}
	server.Start()
}