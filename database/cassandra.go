// Package database implements cassandra connection and queries.
package database

import (
	"log"
	"fmt"
	"time"
	"os"
	
	// "github.com/spf13/viper"
	"github.com/gocql/gocql"
)

// DBConn returns a cassandra session.
func DBConn() (*gocql.Session, error) {

	fmt.Println("Connecting to Cassandra")
	
	// cluster := gocql.NewCluster(viper.GetString("cql_host"))
	cluster := gocql.NewCluster(os.Getenv("CQL_HOST"))
	cluster.ProtoVersion = 4
	// pass := gocql.PasswordAuthenticator{Username: viper.GetString("cql_user"), Password:viper.GetString("cql_pass")}
	pass := gocql.PasswordAuthenticator{Username: os.Getenv("CQL_USERNAME"), Password: os.Getenv("CQL_PASS")}
	cluster.Authenticator = pass
	cluster.Keyspace = os.Getenv("CQL_KEYSPACE")
	cluster.Timeout = 60 * time.Second
	cluster.ConnectTimeout = 10 * time.Second
	cql, err := cluster.CreateSession()

	if err != nil {
		log.Fatalf("%s", err)
		return nil, err
	}
	return cql, nil
}
