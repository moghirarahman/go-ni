package database

import (
	"log"
	
	"github.com/ni/models"
	"github.com/gocql/gocql"
)
// CheckinStore implements database operations for checkins by user.
type CheckinStore struct {
	cql *gocql.Session
}

// NewCheckinStore returns an CheckinStore.
func NewCheckinStore(cql *gocql.Session) *CheckinStore {
	return &CheckinStore{
		cql: cql,
	}
}

// Insert checkin data.
func (s *CheckinStore) Insert(chkin *models.Checkin) error {
	if err := s.cql.Query("INSERT INTO checkin (checkinid, userid, placeid, name, lat,lng, category, checkintimestamp) VALUES (?,?,?,?,?,?,?,?)", gocql.TimeUUID(), chkin.UserID, chkin.PlaceID, chkin.Name, chkin.Latitude, chkin.Longitude, chkin.Category, chkin.CheckinTimestamp).Exec(); err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}
