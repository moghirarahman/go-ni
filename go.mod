module github.com/ni

go 1.12

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-ozzo/ozzo-validation v3.5.0+incompatible
	github.com/gocql/gocql v0.0.0-20190629212933-1335d3dd7fe2
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.4.0
)
