package models

import (
	"time"
	"github.com/go-ozzo/ozzo-validation"
)

// Checkin holds user location data
type Checkin struct {
	UserID string `json:"userId"`
	PlaceID   string  `json:"placeId"`
	Name      string  `json:"name"`
	Longitude float64 `json:"lng"`
	Latitude  float64 `json:"lat,omitempty"`
	Category  string  `json:"category"`
	CheckinTimestamp time.Time `json:"checkinTimestamp"`
}

// Validate validates Checkin struct and returns validation errors.
func (c *Checkin) Validate() error {

	return validation.ValidateStruct(c,
		validation.Field(&c.PlaceID, validation.Required),
		validation.Field(&c.UserID, validation.Required),
		validation.Field(&c.Longitude, validation.Required),validation.Field(&c.Latitude, validation.Required),
		validation.Field(&c.CheckinTimestamp, validation.Required),
	)
}
