package app

import (
	"fmt"
	"net/http"
	// "io/ioutil"
	"errors"
	// "encoding/json"
	
	"github.com/ni/models"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	validation "github.com/go-ozzo/ozzo-validation"
)

// The list of error types returned from checkin resource
var (
	ErrCheckinValidation = errors.New("checkin data validation error")
)

// CheckinStore defines database operations for checkin.
type CheckinStore interface {
	Insert(*models.Checkin) error
}

// CheckinResource implements checkin handler.
type CheckinResource struct {
	Store CheckinStore
}

// NewCheckinResource creates and returns checkin resource.
func NewCheckinResource(store CheckinStore) *CheckinResource {
	return &CheckinResource{
		Store: store,
	}
}

func (rs *CheckinResource) router() *chi.Mux {
	r := chi.NewRouter()
	r.Post("/", rs.insert)
	return r
}


type checkinRequest struct {
	*models.Checkin
}

func (d *checkinRequest) Bind(r *http.Request) error {
	return nil
}

type checkinResponse struct {
	*models.Checkin
}

func newCheckinResponse(c *models.Checkin) *checkinResponse {
	return &checkinResponse{
		Checkin: c,
	}
}

func (rs *CheckinResource) insert(w http.ResponseWriter, r *http.Request) {

	var chkin *models.Checkin
	
	if jsonErr := render.DecodeJSON(r.Body, &chkin); jsonErr != nil {
		// Handling JSON parsing error
		render.Render(w, r, ErrInvalidRequest(jsonErr))
		return
	}

	if err := chkin.Validate(); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	// log(r).WithField("type", "request").Info(chkin)

	if err := rs.Store.Insert(chkin); err != nil {
		switch err.(type) {
		case validation.Errors:
			render.Render(w, r, ErrValidation(ErrCheckinValidation, err.(validation.Errors)))
			return
		}
		fmt.Print("other error", err)

		render.Render(w, r, ErrRender(err))
		return
	}

	resp := newCheckinResponse(chkin)
	// log(r).WithField("type", "response").Infoln(resp.Checkin)

	render.Respond(w, r, resp)
}

