package app

import (
	"net/http"
	
	"github.com/sirupsen/logrus"
	"github.com/ni/database"
	"github.com/gocql/gocql"
	"github.com/go-chi/chi"
	"github.com/ni/logging"
)

// API provides application resources and handlers.
type API struct {
	Checkin *CheckinResource
}

// NewAPI configures and returns application API.
func NewAPI(db *gocql.Session) (*API, error) {
	checkinStore := database.NewCheckinStore(db)
	checkin := NewCheckinResource(checkinStore)

	api := &API{
		Checkin: checkin,
	}
	return api, nil
}
// Router provides application routes.
func (a *API) Router() *chi.Mux {
	r := chi.NewRouter()

	r.Mount("/checkin", a.Checkin.router())

	return r
}

func log(r *http.Request) logrus.FieldLogger {
	return logging.GetLogEntry(r)
}
