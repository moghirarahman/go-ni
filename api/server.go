package api

import (
	"context"
	"net/http"
	"log"
	"os"
	"os/signal"
)

// Server struct with http.Server
type Server struct {
	*http.Server
}

// NewServer Configuration
func NewServer() (*Server, error) {
	handler, err := NewHandler()
	if err != nil {
		return nil, err
	}
	port := os.Getenv("PORT")
	var addr string
	addr = ":" + port
	
	srv := http.Server{
		Addr:    addr,
		Handler: handler,
	}

	return &Server{&srv}, nil
}

// Start handles server startup and graceful shutdown
func (srv *Server) Start() {
	log.Println("Server Startup!")

	go func() {
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			panic(err)
		}
	}()
	log.Printf("Listening on %s\n", srv.Addr)

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	sig := <-quit
	log.Println("Server Shutdown! ", sig)

	if err := srv.Shutdown(context.Background()); err != nil {
		panic(err)
	}
	log.Println("Server gracefully stopped")
}